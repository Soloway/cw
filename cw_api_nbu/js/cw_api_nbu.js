const kurs = new XMLHttpRequest();

kurs.open("GET", "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json");
kurs.onreadystatechange = () => {
    if (kurs.readyState == 4 && kurs.status == 200) {
        const data = JSON.parse(kurs.responseText);
show(data)
    }
}
kurs.send();
const show = function (jsonObj) {
    const name = document.querySelector('.name');
    const rate = document.querySelector('.rate');
    const date = document.querySelector('.date');
    jsonObj.forEach(element => {
       name.innerHTML += `${element.txt} <br>`;
       rate.innerHTML += `${element.rate.toFixed(2)} <br>`;
       date.innerHTML += `${element.exchangedate} <br>`;
    });
}